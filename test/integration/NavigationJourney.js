/** global Qunit, opatest */

sap.ui.define([
    "sap/ui/demo/walkthrough/localService/mockserver",
    "sap/ui/test/opaQunit",
    "./pages/App"
], function (mockserver
) {
    "use strict";

    QUnit.module("Navigation");

    opaTest("Should open the Hello dialog", function (Given, When, Then) {
        // initialize the mock server
        mockserver.init();

        // Arraygements
        Given.iStartMyUIComponent({
            componentConfig: {
                name: "sap.ui.demo.walkthrough"
            }
        });
        // Action
        When.onTheAppPage.iPressTheSayHelloWithDialogButton();

        //assertions
        Then.onTheAppPage.iShouldSeeTheHelloDialog();

        // CleanUp
        Then.iTeardownMyApp();
    });
});