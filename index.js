sap.ui.define([
	"sap/ui/core/ComponentContainer"
], function (ComponentContainer) {
	"use strict";

	new ComponentContainer({
		name: "sap.edivan",
		settings : {
			id: "edivan",
		},
		async: true
	}).placeAt("content");
});
