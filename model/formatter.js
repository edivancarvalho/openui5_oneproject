sap.ui.define([], function () {
    'use strict';
    return {
        statusText: function (sStatus) {
            var resourceBundle = this.getView().getModel("i18n").getResourceBundle();
            switch (sStatus) {
                case "A":
                    return resourceBundle.getText("Solteiro");
                case "B":
                    return resourceBundle.getText("Casado");
                case "C":
                    return resourceBundle.getText("Viuvo");

                default:
                    return sStatus;
            }
        }
    };
});