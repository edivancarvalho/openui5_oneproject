sap.ui.define([
	"sap/ui/core/mvc/Controller",

], function (
	Controller) {
	"use strict";

	return Controller.extend("sap.edivan.controller.Cliente", {

		IrParaCliente: function () {
            var oRouter = this.getOwnerComponent().getRouter();
            oRouter.navTo("IrParaCliente");
        }

	});
});